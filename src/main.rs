use bevy::prelude::*;
use bevy_ecs_tilemap::prelude::*;

use game::board::{Board, BoardHistory};
use game::{mouse_click, BoardClickEvent, CheckEvent, IsBlackTurn};
use menu::{button_interactions, despawn_main_menu, spawn_main_menu};
use tilemap::board::create_board_tilemap;
use tilemap::checked_tile::{spawn_checked_tile, CheckedTileHandle};
use tilemap::hover::{show_hover_ring, spawn_hover_ring};
use tilemap::move_indicators::{spawn_move_indicators, MoveIndicatorHandle, TakeIndicatorHandle};
use tilemap::pieces::{draw_piece_tilemap, ChessPieceHandle};
use tilemap::ranks_and_files::create_labels;
use tilemap::turn_indicator::{draw_turn_indicators, spawn_turn_indicators};
use utils::cursor::{update_cursor_pos, CursorPos};
use utils::on_window_resize;

mod game;
mod menu;
mod tilemap;
mod utils;

#[derive(Clone, Copy, Debug, Default, Eq, Hash, PartialEq, States)]
pub enum AppState {
    #[default]
    MainMenu,
    Game,
}

fn spawn_camera(mut commands: Commands) {
    commands.spawn(Camera2dBundle {
        transform: Transform {
            translation: Vec3::new(-48.0, -48.0, 999.9),
            ..default()
        },
        ..default()
    });
}

fn main() {
    App::new()
        .add_plugins(
            DefaultPlugins
                .set(WindowPlugin {
                    primary_window: Some(Window {
                        title: String::from("Chess"),
                        resolution: (1600., 900.).into(),
                        ..default()
                    }),
                    ..default()
                })
                .set(ImagePlugin::default_nearest()),
        )
        .add_state::<AppState>()
        .init_resource::<CursorPos>()
        .init_resource::<Board>()
        .init_resource::<BoardHistory>()
        .init_resource::<IsBlackTurn>()
        .init_resource::<MoveIndicatorHandle>()
        .init_resource::<TakeIndicatorHandle>()
        .init_resource::<ChessPieceHandle>()
        .init_resource::<CheckedTileHandle>()
        .add_event::<BoardClickEvent>()
        .add_event::<CheckEvent>()
        .add_plugins(TilemapPlugin)
        .add_systems(Startup, spawn_camera)
        .add_systems(Update, on_window_resize)
        .add_systems(OnEnter(AppState::MainMenu), spawn_main_menu)
        .add_systems(
            Update,
            button_interactions.run_if(in_state(AppState::MainMenu)),
        )
        .add_systems(OnExit(AppState::MainMenu), despawn_main_menu)
        .add_systems(
            OnEnter(AppState::Game),
            (
                create_board_tilemap,
                apply_deferred,
                (create_labels, spawn_hover_ring, spawn_turn_indicators),
            )
                .chain(),
        )
        .add_systems(
            Update,
            (
                draw_piece_tilemap,
                draw_turn_indicators,
                spawn_move_indicators,
                spawn_checked_tile,
                update_cursor_pos,
            )
                .run_if(in_state(AppState::Game)),
        )
        .add_systems(
            Update,
            (show_hover_ring, mouse_click)
                .after(update_cursor_pos)
                .run_if(in_state(AppState::Game)),
        )
        .run();
}
