use bevy::prelude::*;

#[derive(Deref, Resource)]
pub struct CursorPos(Vec2);
impl Default for CursorPos {
    fn default() -> Self {
        Self(Vec2::new(-1000., -1000.))
    }
}

pub fn update_cursor_pos(
    camera_q: Query<(&GlobalTransform, &Camera)>,
    mut cursor_moved_events: EventReader<CursorMoved>,
    mut cursor_pos: ResMut<CursorPos>,
) {
    let (camera_transform, camera) = camera_q.single();

    for cursor_moved in cursor_moved_events.iter() {
        if let Some(pos) = camera.viewport_to_world_2d(camera_transform, cursor_moved.position) {
            *cursor_pos = CursorPos(pos);
        }
    }
}
