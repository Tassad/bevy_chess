use bevy::{app::AppExit, prelude::*};

use crate::AppState;

#[derive(Component)]
pub enum ButtonType {
    SinglePlayer,
    MultiPlayer,
    Exit,
}

#[derive(Component)]
pub struct OnMainMenu;

pub fn spawn_main_menu(mut commands: Commands, asset_server: Res<AssetServer>) {
    commands
        .spawn((
            NodeBundle {
                style: Style {
                    width: Val::Percent(100.),
                    height: Val::Percent(100.),
                    flex_direction: FlexDirection::Column,
                    align_items: AlignItems::Center,
                    justify_content: JustifyContent::SpaceBetween,
                    ..default()
                },
                ..default()
            },
            OnMainMenu,
        ))
        .with_children(|parent| {
            parent
                .spawn(NodeBundle {
                    style: Style {
                        width: Val::Percent(100.),
                        height: Val::Percent(100.),
                        flex_direction: FlexDirection::Column,
                        align_items: AlignItems::Center,
                        justify_content: JustifyContent::SpaceEvenly,
                        ..default()
                    },
                    ..default()
                })
                .with_children(|parent| {
                    parent.spawn(
                        TextBundle::from_section(
                            "Chess",
                            TextStyle {
                                font: asset_server.load("fonts/NotoSansMono-Medium.ttf"),
                                font_size: 256.,
                                color: Color::WHITE,
                            },
                        )
                        .with_text_alignment(TextAlignment::Center),
                    );
                });
            parent
                .spawn(NodeBundle {
                    style: Style {
                        width: Val::Percent(100.),
                        height: Val::Percent(100.),
                        flex_direction: FlexDirection::Column,
                        align_items: AlignItems::Center,
                        justify_content: JustifyContent::SpaceEvenly,
                        ..default()
                    },
                    ..default()
                })
                .with_children(|parent| {
                    parent
                        .spawn((
                            ButtonBundle {
                                style: Style {
                                    padding: UiRect::horizontal(Val::Px(10.)),
                                    ..default()
                                },
                                ..default()
                            },
                            ButtonType::SinglePlayer,
                        ))
                        .with_children(|parent| {
                            parent.spawn(
                                TextBundle::from_section(
                                    "Vs. Self",
                                    TextStyle {
                                        font: asset_server.load("fonts/NotoSansMono-Medium.ttf"),
                                        font_size: 72.,
                                        color: Color::BLACK,
                                    },
                                )
                                .with_text_alignment(TextAlignment::Center),
                            );
                        });
                    parent
                        .spawn((
                            ButtonBundle {
                                style: Style {
                                    padding: UiRect::horizontal(Val::Px(10.)),
                                    ..default()
                                },
                                ..default()
                            },
                            ButtonType::MultiPlayer,
                        ))
                        .with_children(|parent| {
                            parent.spawn(
                                TextBundle::from_section(
                                    "Online Versus",
                                    TextStyle {
                                        font: asset_server.load("fonts/NotoSansMono-Medium.ttf"),
                                        font_size: 72.,
                                        color: Color::BLACK,
                                    },
                                )
                                .with_text_alignment(TextAlignment::Center),
                            );
                        });
                    parent
                        .spawn((
                            ButtonBundle {
                                style: Style {
                                    padding: UiRect::horizontal(Val::Px(10.)),
                                    ..default()
                                },
                                ..default()
                            },
                            ButtonType::Exit,
                        ))
                        .with_children(|parent| {
                            parent.spawn(
                                TextBundle::from_section(
                                    "Exit",
                                    TextStyle {
                                        font: asset_server.load("fonts/NotoSansMono-Medium.ttf"),
                                        font_size: 72.,
                                        color: Color::BLACK,
                                    },
                                )
                                .with_text_alignment(TextAlignment::Center),
                            );
                        });
                });
        });
}

#[allow(clippy::type_complexity)]
pub fn button_interactions(
    mut app_state: ResMut<NextState<AppState>>,
    mut app_exit_ev: EventWriter<AppExit>,
    interaction_query: Query<(&Interaction, &ButtonType), (Changed<Interaction>, With<Button>)>,
) {
    for (interaction, button_type) in interaction_query.iter() {
        if *interaction == Interaction::Pressed {
            match button_type {
                ButtonType::SinglePlayer => app_state.set(AppState::Game),
                ButtonType::MultiPlayer => todo!("multiplayer"),
                ButtonType::Exit => app_exit_ev.send(AppExit),
            }
        }
    }
}

pub fn despawn_main_menu(mut commands: Commands, query: Query<Entity, With<OnMainMenu>>) {
    commands.entity(query.single()).despawn_recursive();
}
